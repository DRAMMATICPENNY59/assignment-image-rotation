//
// Created by leonid on 01.11.22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGETHINGS_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGETHINGS_H

#include <inttypes.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGETHINGS_H
