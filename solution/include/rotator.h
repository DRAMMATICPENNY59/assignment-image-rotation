//
// Created by leonid on 31.10.22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATOR_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATOR_H

#include "bmp.h"
#include "imageThings.h"

struct image rotate_90_degree(struct image image);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATOR_H
