//
// Created by leonid on 01.11.22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MEMORY_H
#define ASSIGNMENT_IMAGE_ROTATION_MEMORY_H

#include "bmp.h"
#include "imageThings.h"

void imageRemoverF( struct image * img);

struct image imageCreatorF(uint32_t width, uint32_t height);

#endif //ASSIGNMENT_IMAGE_ROTATION_MEMORY_H
