//
// Created by leonid on 31.10.22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include  <stdint.h>
#include <bits/types/FILE.h>
#include "imageThings.h"

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};


enum read_status bmpReaderF(const char *input_file, struct image *img);

enum write_status bmpWriterF(const char *output_file, const struct image img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
