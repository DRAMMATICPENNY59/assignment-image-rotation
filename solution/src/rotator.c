//
// Created by leonid on 31.10.22.
//

//
// Created by leonid on 31.10.22.
//

#include "rotator.h"
#include "memory.h"

struct pixel getPixelC(uint64_t x, uint64_t y, const struct image *img) {
    return img->data[x + y * img->width];
}

void setPixelC(uint64_t x, uint64_t y, const struct image *img, struct pixel p) {
    img->data[x + y * img->width] = p;
}

struct image rotate_90_degree(struct image image) {
    struct image n_image = imageCreatorF(image.height, image.width);

    for (uint64_t i = 0; i < image.height; i++) {
        for (uint64_t j = 0; j < image.width; j++) {
//            n_image->data[j * image.height + (image.height - 1 - i)] = image.data[i * image.width + j];
            setPixelC(image.height - 1 - i, j, &n_image, getPixelC(j, i, &image));
        }
    }
    return n_image;
}
