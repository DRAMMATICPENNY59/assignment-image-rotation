//
// Created by leonid on 31.10.22.
//
#include "memory.h"
#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint32_t padd(uint32_t width) {
    return (4 - (width * 3 % 4)) % 4;
}

struct bmp_header headerMaker(const struct image img) {
    const struct bmp_header HEADER = {
            .bfType = 0x4D42,
            .biSize = 40,
            .biPlanes = 1,
            .bfileSize = img.width * img.height * sizeof(struct pixel) + padd(img.width) * img.height, // Size of image
            .biWidth = img.width,
            .biHeight = img.height,
            .biBitCount = sizeof(struct pixel) * 8,
            .bOffBits = sizeof(struct bmp_header)};
    return HEADER;
}

enum read_status headerReader(FILE *f_in, struct bmp_header *header) {
    fread(header, sizeof(struct bmp_header), 1, f_in);
    if (header->bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biCompression != 0) {
        return READ_INVALID_HEADER;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status bmpReaderF(const char *input_file, struct image *img) {
    FILE *f_in = fopen(input_file, "rb");
    if (f_in == NULL) {
        return READ_INVALID_HEADER;
    }
    struct bmp_header bmpHeader;
    enum read_status status = headerReader(f_in, &bmpHeader);
    if (status != READ_OK) {
        fclose(f_in);
        return status;
    }

    *img = imageCreatorF(bmpHeader.biWidth, bmpHeader.biHeight);

    const uint32_t PADDING = padd(bmpHeader.biWidth);

    fseek(f_in, bmpHeader.bOffBits, SEEK_SET);
    for (size_t i = 0; i < img->height; ++i) {
        fread(img->data + img->width * i, sizeof(struct pixel), img->width, f_in);
        fseek(f_in, PADDING, SEEK_CUR);
    }
    fclose(f_in);
    return READ_OK;
}

enum write_status bmpWriterF(const char *output_file, const struct image img){
    FILE *f_out = fopen(output_file, "wb");
    if (f_out == NULL) {
        return WRITE_ERROR;
    }
    struct bmp_header header = headerMaker(img);
    fwrite(&header, sizeof(struct bmp_header), 1, f_out);

    const uint32_t PADDING = padd(img.width);

    for (size_t i = 0; i < img.height; ++i) {
        fwrite(img.data + img.width * i, sizeof(struct pixel), img.width, f_out);
        fwrite(img.data, 1, PADDING, f_out);
    }
    fclose(f_out);
    return WRITE_OK;
}
