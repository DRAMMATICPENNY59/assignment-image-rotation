#include "bmp.h"
#include "imageThings.h"
#include "memory.h"
#include "rotator.h"
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    if( argc != 3){
        return 1;
    } else{
        printf("%s %s \n",argv[1],argv[2]);
    }


    struct image new_image;

    enum read_status rstatus  = bmpReaderF(argv[1], &new_image);

    if(rstatus == READ_OK){
        printf("read ok \n");
    } else{
        printf("read not ok \n");
        return 1;
    }

    struct image rotate = rotate_90_degree(new_image);
    imageRemoverF(&new_image);

    enum write_status wstatus  = bmpWriterF(argv[2], rotate);
    imageRemoverF(&rotate);

    if(wstatus == WRITE_OK){
        printf("write ok \n");
    } else{
        printf("write not ok \n");
        return 1;
    }


    return 0;
}
