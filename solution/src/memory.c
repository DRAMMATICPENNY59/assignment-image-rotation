//
// Created by leonid on 01.11.22.
//

#include "memory.h"
#include <stdlib.h>

void imageRemoverF(struct image *img) {
    free(img->data);
}

struct image imageCreatorF(uint32_t width, uint32_t height){
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel));
    return img;
}
